from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response
import requests
import json

class DetailPageUnitTest(TestCase) :
    def test_terdapat_respon_http_yang_tepat(self) :
        response = Client().get('/search/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/search/data/?item=thereisnobookwiththisname', **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"kind": "books#volumes", "totalItems": 0})
        
        
    def test_url_accordion_menggunakan_template_dan_juga_base_templatenya(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response,"story8/search.html")
        self.assertTemplateUsed(response,"home/base-solidnavbar.html")
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from .forms import MatkulForm
from .models import Matkul

def index(request):
    return render(request, 'home/index.html')
    
def likes(request):
    return render(request, 'home/likes.html')

def firstHTML(request):
    return render(request, 'home/1st-html.html')

def matkul(request):
    Matkuls = Matkul.objects.all()
    
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'home/matkul.html', {'form': form, 'matkuls': Matkuls})
    
    form = MatkulForm()
    return render(request, 'home/matkul.html', {'form': form, 'matkuls': Matkuls})

def matkulDetail(request,id):
    matkul = Matkul.objects.get(id=id)
    Matkuls = Matkul.objects.all()
    return render(request,'home/matkul-detail.html',{'matkul':matkul, 'matkuls': Matkuls})

def deleteMatkul(request,id):
    matkul = Matkul.objects.get(id=id)
    if request.method == 'POST' :
        matkul.delete()
        return redirect('/matkul')
    return render(request, 'home/matkul-delete.html',{'matkul':matkul})

from django.shortcuts import render, redirect
from .forms import RegisterForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm


def Story9(request):
    return render(request, "story9/story9.html")


def Register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST);
        if form.is_valid():
            form.save();
            return redirect('/djangologin/')
    else:
        form = RegisterForm();
    return render(request, "story9/register.html", {'form':form})

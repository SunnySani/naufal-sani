# Naufal Sani's Web
Coverage status:

[![coverage report](https://gitlab.com/SunnySani/naufal-sani/badges/master/coverage.svg)](https://gitlab.com/SunnySani/naufal-sani/-/commits/master)

Pipeline status:
[![pipeline status](https://gitlab.com/SunnySani/naufal-sani/badges/master/pipeline.svg)](https://gitlab.com/SunnySani/naufal-sani/-/commits/master)

Link herokuapp: http://naufal-sani.herokuapp.com/
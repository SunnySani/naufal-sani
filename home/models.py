from django.db import models

class Matkul(models.Model):
    name = models.CharField(max_length = 25)
    credit = models.TextField(default=1)
    lecturer = models.TextField(max_length = 25)
    description = models.TextField(blank=True, max_length = 40)
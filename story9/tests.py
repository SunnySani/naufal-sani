from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response
import story9.views

# Create your tests here.

class UnitTest(TestCase):
    def test_url_is_exist(self):
        response = self.client.get('/djangologin/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/djangologin/register/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/djangologin/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/djangologin/logout/')
        self.assertEqual(response.status_code, 302)

    def test_url_using_templates(self):
        response = self.client.get('/djangologin/')
        self.assertTemplateUsed(response, 'story9/story9.html')
        response = self.client.get('/djangologin/register/')
        self.assertTemplateUsed(response, 'story9/register.html')
        response = self.client.get('/djangologin/login/')
        self.assertTemplateUsed(response, 'registration/login.html')


    '''def test_using_correct_template(self):
        response = self.client.get('/djangologin/')
        self.assertTemplateUsed(response, 'story9.html')
        response = self.client.get('/djangologin/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_template_elements(self):
        request = HttpRequest()
        response = userLogin(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Login Page",html_response)

    def test_trying_login(self):
        response = self.client.post('/story9/', data={'user_name' : 'coba', 'pass_word' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'Hello, coba')
    
    def test_trying_register_with_existed_account(self):
        response = self.client.post('/story9/register/', data={'user_name' : 'coba', 'pass_word' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'Login')'''


from django import forms
from .models import Activity, Person

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ('name', 'description',)

class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ('name', 'activity',)
from django.urls import path, include
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.Story9, name='index'),
    path('register/', views.Register, name='register'),
    path('', include('django.contrib.auth.urls'), name='django-auth'),
]
from django.contrib import admin
from .models import Activity, Person

admin.site.register(Activity)
admin.site.register(Person)

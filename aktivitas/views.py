from django.shortcuts import render, redirect
from .forms import ActivityForm, PersonForm
from .models import Activity, Person

def Aktivitas(request):
    kegiatans = Activity.objects.all()
    participants = Person.objects.all()
    listObject = []
    for kegiatan in kegiatans:
        kegiatan_partic = []
        for participant in participants:
            if (str(participant.activity) == str(kegiatan)):
                kegiatan_partic.append(participant)
        listObject.append((kegiatan,kegiatan_partic))    
    context = {
        "listObject" : listObject
    }
    return render(request, 'aktivitas/aktivitas.html', context )

def addObject(request):
    id_kegiatan = request.POST.get('id_kegiatan', None)
    name_participant = request.POST.get('name_participant',None)
    nama_kegiatan = request.POST.get('nama_kegiatan', None)
    deskripsi_kegiatan = request.POST.get('deskripsi_kegiatan', None)

    if (deskripsi_kegiatan) :
        Activity.objects.create(name=nama_kegiatan, description=deskripsi_kegiatan)
        # Person.objects.create(name=name_participant, activity=Activity.objects.get(id = id_kegiatan))
    else:
        Person.objects.create(name=name_participant, activity=Activity.objects.get(id = id_kegiatan))
        # Activity.objects.create(name=nama_kegiatan, description=deskripsi_kegiatan)


    return redirect('/activity')

from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.Search, name='search'),
    path('data/', views.SearchInput, name='searchInput'),
]
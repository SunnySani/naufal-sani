from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('activity/', include('aktivitas.urls')),
    path('accordion/', include('my_jquery.urls')),
    path('search/', include('story8.urls')),
    path('djangologin/', include('story9.urls')),
]
from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        widgets = {
        'name': forms.TextInput(attrs={'size': 40}), 
        'lecturer': forms.TextInput(attrs={'size': 40}), 
        'credit': forms.TextInput(attrs={'size': 40}),
        'description': forms.TextInput(attrs={'size': 40}),
        }
        fields = ('name', 'credit', 'lecturer', 'description')
$(document).ready(function() { 
    var panels = $('.accordion .description').hide();
    var prev = null;
    
    $('.accordion .content .title a').click(function() {
        var $this = $(this);
        
        if (this == prev) {
            $this.parent().next().slideToggle();
        } else {
            // slide every panels
            panels.slideUp();
            // slide down target panel
            $this.parent().next().slideDown();
        }
        prev = this;

        return false;
    });
    
    $('.accordion > .content > .title > .upbutton').click(function() {
        var divPointed = $(this).parent().parent();
        divPointed.after(divPointed.prev());
        return false;
    });
    
    $('.accordion > .content > .title > .downbutton').click(function() {
        var divPointed = $(this).parent().parent();
        divPointed.before(divPointed.next());
        return false;
    });
});
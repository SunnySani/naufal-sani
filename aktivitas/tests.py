from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response

from aktivitas.models import Activity, Person
import aktivitas.views 


#How to test: coverage run --source='aktivitas' manage.py test aktivitas
class DetailPageUnitTest(TestCase) :
    def test_aktivitas_terdapat_url_dengan_HTTP_respon_yang_tepat (self) :
        response = Client().get('/activity/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/activity/add')
        self.assertEqual(response.status_code, 301)
        
    def test_aktivitas_menggunakan_template(self):
        activity = Activity.objects.create(name="testAktivitas")
        person = Person.objects.create(name="testNama",activity=activity)
    
        response = Client().get('/activity/')
        self.assertTemplateUsed(response,"home/base-solidnavbar.html")

    def test_aktivitas_dapat_membuat_objek_activity (self) :
        activity = Activity.objects.create(name="test")
        count = Activity.objects.all().count()
        self.assertEqual(count, 1)

    def test_aktivitas_dapat_membuat_objek_person (self):
        activity = Activity.objects.create(name="testAktivitas")
        person = Person.objects.create(name="testNama",activity=activity)
        self.assertEquals(str(person.name),"testNama")
        self.assertEquals(str(person.activity.name),"testAktivitas")

    def test_aktivitas_menyimpan_activity_saat_POST_di_web(self):
        response = Client().post('/activity/add/', data={
            "nama_kegiatan" : "test", "deskripsi_kegiatan" : "Default"})
        count = Activity.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(response.status_code, 302)
    
    def test_aktivitas_menyimpan_person_saat_POST_di_web(self):
        activity = Activity.objects.create(name="testActivity")
        activity1 = Activity.objects.create(name="testActivity1")
        response = Client().post('/activity/add/',data= {
            "id_kegiatan" : "1",
            "name_participant" : "test"
        })
        count = Person.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(response.status_code,302)




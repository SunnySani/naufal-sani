from django.db import models

class Activity(models.Model):
    name = models.CharField(max_length = 25)
    description = models.TextField(max_length=500, blank=True)
    
class Person(models.Model):
    name = models.CharField(max_length = 25)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE) #Many to one

from django.urls import path
from . import views

app_name = 'accordion'

urlpatterns = [
    path('', views.Accordion, name='accordion'),
]
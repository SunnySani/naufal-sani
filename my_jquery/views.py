from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404

def Accordion(request):
    return render(request, 'my_jquery/accordion.html')

from django.urls import path
from . import views

app_name = 'aktivitas'

urlpatterns = [
    path('', views.Aktivitas),
    path('add/', views.addObject, name='addObject'),
]
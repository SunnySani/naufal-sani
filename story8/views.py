from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
import requests

def Search(request):
    return render(request, 'story8/search.html')

def SearchInput(request):
    if(request.is_ajax()):
        response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + request.GET['item'])
        data = response.json()
        return JsonResponse(data)
from django.apps import AppConfig


class MyJqueryConfig(AppConfig):
    name = 'my_jquery'

from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('likes/', views.likes, name='likes'),
    path('1st-html/', views.firstHTML, name='firstHTML'),
    path('matkul/', views.matkul, name='matkul'),
    path('matkul/<int:id>',views.matkulDetail),
    path('matkul/delete-course/<int:id>', views.deleteMatkul, name='deleteMatkul'),
]